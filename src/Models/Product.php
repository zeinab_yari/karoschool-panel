<?php


namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'price',
        'discounted_price',
        'view',
        'stock',
        'main_image',
        'gallery',
        'description',
    ];

}
