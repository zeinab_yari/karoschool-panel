<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" id="{{ config('app.locale') == 'fa' ? 'rtl' : 'ltr' }}">
<head>

    <meta charset="UTF-8">
    <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

{{--    <link href="{{ voyager_asset('style/app.css') }}" rel="stylesheet">--}}
    <link href="{{ voyager_asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ voyager_asset('css/master.css') }}" rel="stylesheet">
    <link href="{{ voyager_asset('css/voyager-font.css') }}" rel="stylesheet">
    <link href="{{ voyager_asset('style/newStyle.css') }}" rel="stylesheet">
    <script src="{{ voyager_asset('js/JQUERY.js') }}"></script>
    <script src="{{ voyager_asset('js/BOOTSTRAP.js') }}"></script>
    <script src="{{ voyager_asset('js/ajax.js') }}"></script>
    <script src="{{ voyager_asset('js/chart.js') }}"></script>

    <!--    --><?php //$admin_favicon = Voyager::setting('admin.icon_image', ''); ?>
    {{--@if($admin_favicon == '')
        <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/png">
    @else
        <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">
    @endif--}}

    @yield('css')

    @if(!empty(config('voyager.additional_css')))
        @foreach(config('voyager.additional_css') as $css)
            <link rel="stylesheet" type="text/css" href="{{ asset($css) }}">
        @endforeach
    @endif

    @yield('head')

</head>

<body>

<div class="container-fluid">
    <div class="overlay">

    </div>
    <div class="row ">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="container">
                @include('voyager::dashboard.sidebar')
                <div class="main">
                    <div id="nav-toggle">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="row">
                        @yield('search')

                        {{--<div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <nav class="navigation">
                                @include('voyager::dashboard.navbar')
                                <div class="right-top-nav">
                                    <form>
                                        <img class="arrow" src="{{ voyager_asset('icon/arrow.svg') }}">
                                        <img class="search" src="{{ voyager_asset('icon/search.svg') }}">
                                        <div class="search-box">
                                            <input class="search-input" type="text"
                                                   placeholder="جستجو در لیست کاربران ">
                                            <ul>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در تراکنش ها</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                             نام کاربر
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در محصولات</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                           نام محصول
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در ویترین</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                           عنوان ویترین
                                                        </span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="flex-box">
                                                        <span class="p-item">جستجو در سرویس ها</span>
                                                        <img src="assets/icon/arrow.svg">
                                                        <span>
                                                           عنوان سرویس
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    </form>
                                </div>
                            </nav>
                        </div>--}}
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="row">

                                @yield('content')

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ voyager_asset('js/master.js') }}"></script>
<script src="{{ voyager_asset('js/dropdown.js') }}"></script>
<script src="{{ voyager_asset('js/nprogress.js') }}"></script>


<script
    src="{{ Auth::user()->locale ? voyager_asset('js/main_'.Auth::user()->locale.'.js') : voyager_asset('js/main_'.Config::get('app.locale').'.js') }}"></script>
<script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>
<script src="{{ voyager_asset('js/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>

<script src="{{ voyager_asset("js/multilingual.js") }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.6/tinymce.min.js"></script>


@include('voyager::media.manager')

@yield('javascript')

@if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
    @foreach(config('voyager.additional_js') as $js)
        <script type="text/javascript" src="{{ voyager_asset($js) }}"></script>
    @endforeach
@endif

</body>
</html>
