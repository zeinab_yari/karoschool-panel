@section('database-column-template')

    <tr class="tr" >
        <td class="padding-item child2"  data-column="نام">

            <div class=" input-row">

                <input :value="column.name" @input="onColumnNameInput" type="text" required pattern="{{ $db->identifierRegex }}" placeholder="نام جدول "/>

            </div>
        </td>
        <td class="padding-item child2" data-column="نوع">

            <database-types
                :column="column"
                @typeChanged="onColumnTypeChange">
            </database-types>
{{--            check And complete this Part--}}
        </td>
        <td class="padding-item child1" data-column="طول">

            <div class="count-number input-row">

                <input step="1"  v-model.number="column.length" :type="lengthInputType" min="0"/>
                <div class="quantity__btn quantity__btn--up">
                    <img src="{{ voyager_asset('icon/black-arrow.svg') }}">
                </div>
                <div class="quantity__btn quantity__btn--down">
                    <img src="{{ voyager_asset('icon/black-arrow.svg') }}">
                </div>

            </div>
        </td>
        <td class="padding-item child1 text-center
" data-column="Not Null">
            <div class="check-box edit-checkbox">
                <input v-model="column.notnull" class="my-checkbox" type="checkbox" id="NotNull1">
                <label for="NotNull1">
                    <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">
                </label>
            </div>
        </td>
        <td class="padding-item child1 text-center
" data-column="Unsigned">
            <div class="check-box edit-checkbox">
                <input v-model="column.unsigned" class="my-checkbox" type="checkbox" id="Unsigned1">
                <label for="Unsigned1">
                    <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">
                </label>
            </div>
        </td>
        <td class="padding-item child2 text-center
" data-column="Auto Increment">
            <div class="check-box edit-checkbox">
                <input v-model="column.autoincrement" class="my-checkbox" type="checkbox" id="AutoIncrement1">
                <label for="AutoIncrement1">
                    <img src="{{ voyager_asset('icon/white-checkbox.svg') }}">
                </label>
            </div>
        </td>
        <td class="padding-item child1 text-center
" data-column="Index">



            <div class=" input-row">

                <div class="dropdown custom-selects" >
                    <select :value="index.type" @change="onIndexTypeChange"
                            :disabled="column.type.notSupportIndex"
                            class="form-control">
                        <option value=""></option>
                        <option value="INDEX">{{ __('voyager::database.index') }}</option>
                        <option value="UNIQUE">{{ __('voyager::database.unique') }}</option>
                        <option value="PRIMARY">{{ __('voyager::database.primary') }}</option>
                    </select>
                </div>
                <small v-if="column.composite" v-once>{{ __('voyager::database.composite_warning') }}</small>
            </div>
        </td>

        <td class="padding-item child2 text-center">
            <div class="flex-box button-row">
                <database-column-default :column="column"></database-column-default>

                <a class="account-number " @click="deleteColumn">
                    <img src="{{voyager_asset('icon/trash.svg')}}">
                </a>
            </div>
        </td>
    </tr>


@endsection

@include('voyager::tools.database.vue-components.database-types')
@include('voyager::tools.database.vue-components.database-column-default')

<script>
    Vue.component('database-column', {
        data: function() {
            return {
                lengthInputType: 'number'
            }
        },
        props: {
            column: {
                type: Object,
                required: true
            },
            index: {
                type: Object,
                required: true
            }
        },
        template: `@yield('database-column-template')`,
        methods: {
            deleteColumn() {
                this.$emit('columnDeleted', this.column);

                // todo: add an UNDO button or something in case the user mistakenly deletes the column
            },
            onColumnNameInput(event) {
                let newName = event.target.value;

                this.$emit('columnNameUpdated', {
                    column: this.column,
                    newName: newName
                });
            },
            onColumnTypeChange(type) {
                if (type.notSupportIndex && this.index.type) {
                    this.$emit('indexDeleted', this.index);
                }

                // Reset default value
                this.column.default = null;

                this.column.type = type;

                this.setLengthInputType();
            },
            onIndexTypeChange(event) {
                if (this.column.name == '') {
                    return toastr.error("{{ __('voyager::database.name_warning') }}");
                }

                return this.$emit('indexChanged', {
                    columns: [this.column.name],
                    old: this.index,
                    newType: event.target.value
                });
            },
            setLengthInputType() {
                var name = this.column.type.name;
                if (name == 'double' || name == 'float' || name == 'decimal') {
                    this.lengthInputType = 'text';
                } else {
                    this.lengthInputType = 'number';
                }
            }
        },
        mounted() {
            this.setLengthInputType();
        },
    });
</script>
