<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{

    public function up()
    {
        if (!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->id();
                $table->string('title')->nullable();
                $table->string('price')->default(0);
                $table->string('discounted_price')->default(0);
                $table->string('view')->default(0);
                $table->string('stock')->default(0);
                $table->string('sell_num')->default(0);
                $table->string('sells')->default(0);
                $table->text("main_image")->nullable();
                $table->text("gallery")->nullable();
                $table->text('description')->nullable();
                $table->timestamps();
            });
        }
    }


    public function down()
    {
        Schema::dropIfExists('products');
    }
}
