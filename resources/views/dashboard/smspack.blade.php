
@php
    $smsdata = \TCG\Voyager\Models\Sms::first();
@endphp


<div class="col-lg-6 col-md-12 col-sm-12">
    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <h5 class="title">
                {{__('voyager::hotdesk.smspack')}}
            </h5>
        </div>
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <div class="box">
                <div class="row">
                    <div
                        class="second-padding-item col-xl-6 col-lg-12 col-md-6 col-sm-6 col-12">
                        <div class="active massage-panel-item">
                            <p>
                                {{__('voyager::hotdesk.smspack_qu')}}
                            </p>
                            <h2>
                                {{Auth::user()->locale == 'fa' ? fa_number($smsdata->stock) : $smsdata->stock}}
                            </h2>
                        </div>
                    </div>
                    <div
                        class="second-padding-item col-xl-6 col-lg-12 col-md-6 col-sm-6 col-12">
                        <div class="massage-panel-item">
                            <p>
                                {{__('voyager::hotdesk.smspack_totalsends')}}
                            </p>
                            <h2>
                                {{Auth::user()->locale == 'fa' ? fa_number($smsdata->totalsend) : $smsdata->totalsend}}
                            </h2>
                        </div>
                    </div>

{{--
                    <div
                        class="second-padding-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="charge-panel dropdown custom-selects">
                            <select>
                                <option value="0">{{__('voyager::hotdesk.smspack_pay')}}</option>
                                <option value="1">
                                    {{Auth::user()->locale == 'fa' ? '۱۰۰۰' : '1000'}}
                                </option>
                                <option value="2"> {{Auth::user()->locale == 'fa' ? '۵۰۰۰' : '5000'}}</option>
                            </select>
                        </div>


                    </div>
--}}
                </div>
            </div>
        </div>
    </div>
</div>
