<a class="flex-box back" href="{{ url()->previous() }}">
    <img src="{{voyager_asset('icon/back.svg')}}">
    {{ __('voyager::generic.return_to_list') }}

</a><!-- !!! Add form action below -->
<form role="form" action="{{ route('voyager.bread.relationship') }}" method="POST">

    <div class="modal fade show" id="new_relationship_modal" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title"><i class="voyager-heart"></i> {{ \Illuminate\Support\Str::singular(ucfirst($table)) }}
                        {{ __('voyager::database.relationship.relationships') }} </h4>
                </div>
                <div class="modal-body">

                    @if(isset($dataType->id))
                        <div class="col-md-12 relationship_details">
                            <p class="col-md-12 relationship_table_select">{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</p>
                            <div class="flexit">

                               {{-- <div class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12">
                                    <div class="input-row">
                                        <label>
                                            {{ __('voyager::bread.order_column') }}
                                            <span class="voyager-question"
                                                  aria-hidden="true"
                                                  data-toggle="tooltip"
                                                  data-placement="right"
                                                  title="{{ __('voyager::bread.order_column_ph') }}">

                                                            </span>
                                        </label>
                                        <div class="dropdown custom-selects">

                                            <select>
                                                <option value="hasOne" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasOne') selected="selected" @endif>{{ __('voyager::database.relationship.has_one') }}</option>
                                                <option value="hasMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasMany') selected="selected" @endif>{{ __('voyager::database.relationship.has_many') }}</option>
                                                <option value="belongsTo" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsTo') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to') }}</option>
                                                <option value="belongsToMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsToMany') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to_many') }}</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
--}}

                                <div class="form-group select gray input-row" style="background-color: var(--input-bg) ">
                                    <div class="select selectMainBox">
                                        <select class="relationship_type select2 dropdown custom-selects selectBoxBread" name="relationship_type" >
                                            <option value="hasOne" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasOne') selected="selected" @endif >{{ __('voyager::database.relationship.has_one') }}</option>
                                            <option value="hasMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasMany') selected="selected" @endif>{{ __('voyager::database.relationship.has_many') }}</option>
                                            <option value="belongsTo" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsTo') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to') }}</option>
                                            <option value="belongsToMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsToMany') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to_many') }}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group select gray" style="background-color: var(--input-bg) ">
                                    <div class="select selectMainBox">
                                        <select class="relationship_table select2 selectBoxBread" name="relationship_table">
                                            @foreach($tables as $tbl)
                                                <option value="{{ $tbl }}" @if(isset($relationshipDetails->table) && $relationshipDetails->table == $tbl) selected="selected" @endif>{{ \Illuminate\Support\Str::singular(ucfirst($tbl)) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group gray" style="background-color: var(--input-bg) ">
                                    <input type="text" class="form-control" name="relationship_model" placeholder="{{ __('voyager::database.relationship.namespace') }}" value="{{ $relationshipDetails->model ?? ''}}" style="background: var(--input-bg); border: 1px solid var(--pagination-borde)">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 relationship_details relationshipField">
                            <div class="belongsTo">
                                <div class="form-group select gray" style="background-color: var(--input-bg) ">
                                    <label>{{ __('voyager::database.relationship.which_column_from') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span class="label_table_name"></span>?</label>
                                    <div class="select selectMainBox">
                                        <select name="relationship_column_belongs_to" class="new_relationship_field select2 selectBoxBread">
                                            @foreach($fieldOptions as $data)
                                                <option value="{{ $data['field'] }}">{{ $data['field'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="hasOneMany flexed">
                                <div class="form-group select gray" style="background-color: var(--input-bg) ">
                                    <label>{{ __('voyager::database.relationship.which_column_from') }} <span class="label_table_name"></span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span>?</label>
                                    <div class="select selectMainBox">
                                        <select name="relationship_column" class="new_relationship_field select2 rowDrop selectBoxBread" data-table="{{ $tables[0] }}" data-selected="">
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 relationship_details relationshipPivot">
                            <div class="form-group select gray" style="background-color: var(--input-bg) ">
                                <label>{{ __('voyager::database.relationship.pivot_table') }}:</label>
                                <div class="select selectMainBox">
                                    <select name="relationship_pivot" class="select2 selectBoxBread">
                                        @foreach($tables as $tbl)
                                            <option value="{{ $tbl }}" @if(isset($relationshipDetails->table) && $relationshipDetails->table == $tbl) selected="selected" @endif>{{ \Illuminate\Support\Str::singular(ucfirst($tbl)) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 relationship_details_more">
                            <div class="well">
                                <div class="form-group select" style="background-color: var(--input-bg) ">
                                    <label>{{ __('voyager::database.relationship.selection_details') }} <span class="label_table_name"></span></label>

                                    <div class="select selectMainBox">
                                        <select name="relationship_label" class="rowDrop select2 selectBoxBread" data-table="{{ $tables[0] }}" data-selected="">
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group select relationship_key" style="background-color: var(--input-bg) ">
                                    <label>{{ __('voyager::database.relationship.store_the') }} <span class="label_table_name"></span>: </label>
                                    <div class="select selectMainBox">
                                        <select name="relationship_key" class="rowDrop select2 selectBoxBread" data-table="{{ $tables[0] }}" data-selected="">
                                        </select>
                                    </div>
                                </div>

                                <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                                    <div class="flex-box check-box-row">

                                        <label class="switch">
                                            <h6>
                                                {{ __('voyager::database.relationship.allow_tagging') }}
                                            </h6>
                                            <?php $checked = (isset($dataType->generate_permissions) && $dataType->generate_permissions == 1) || (isset($generate_permissions) && $generate_permissions); ?>

                                            <input  type="checkbox"
                                                    name="relationship_taggable"
                                                    data-on="{{ __('voyager::generic.yes') }}"
                                                    data-off="{{ __('voyager::generic.no') }}"
                                                    @if($checked) checked @endif
                                                    hidden />
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </div>

                                {{--<div class="form-group relationship_taggable">
                                    <label>{{ __('voyager::database.relationship.allow_tagging') }}:</label>
                                    <input type="checkbox" name="relationship_taggable" class="toggleswitch" data-on="{{ __('voyager::generic.yes') }}" data-off="{{ __('voyager::generic.no') }}">
                                </div>--}}

                            </div>
                        </div>
                    @else
                        <div class="col-md-12">
                            <h5><i class="voyager-rum-1"></i> {{ __('voyager::database.relationship.easy_there') }}</h5>
                            <p class="relationship-warn">{!! __('voyager::database.relationship.before_create') !!}</p>
                        </div>
                    @endif

                </div>

                <div class="modal-footer">
                    <div class="relationship-btn-container">
                        <button type="button" class="btn btn-default" data-dismiss="modal" style="color: var(--nav-item-hover)">{{ __('voyager::database.relationship.cancel') }}</button>
                        @if(isset($dataType->id))
                            <button class="btn btn-red btn-relationship" style="color: var(--nav-item-hover)"><i class="voyager-plus"></i> <span>{{ __('voyager::database.relationship.add_new') }}</span></button>
                        @endif
                    </div>
                </div>


            </div>
        </div>
    </div>



    <input type="hidden" value="{{ $dataType->id ?? '' }}" name="data_type_id" style="background: var(--input-bg)">
	{{ csrf_field() }}
</form>

<div class="overlay">

    <script>
        $( ".dropdown" ).click(function() {
            $(this).addClass("active");
        });
    </script>

