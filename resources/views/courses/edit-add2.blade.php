@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
    $categories = \App\Models\CourseCategory::all();
    $tools = \App\Models\Tool::all();
    $teachers = \App\Models\Teacher::all();
    $courses = \App\Models\Course::all();
@endphp

@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{voyager_asset('style/rte_theme_default.css')}}"/>
    <script src="{{voyager_asset('js/popper.min.js')}}"></script>

    <link rel="stylesheet" href="{{voyager_asset('richtexteditor/rte_theme_default.css')}}"/>
    <script type="text/javascript" src="{{voyager_asset('richtexteditor/rte.js')}}"></script>
    <script type="text/javascript" src="{{voyager_asset('richtexteditor/all_plugins.js')}}"></script>
@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    @include('voyager::multilingual.language-selector')
@endsection

@section('content')

    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <nav class="navigation">
                <div class="right-top-nav flex-box">
                    <a class="flex-box back">
                        <img src="{{voyager_asset('icon/back.svg')}}"/>
                        برگشت
                    </a>
                    <a class="flex-box add-new" onclick="submit_button()">
                        <img src="{{voyager_asset('icon/plus.svg')}}"/>
                        <p>ثبت و افزودن دوره جدید</p>
                    </a>
                </div>
            </nav>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 errorBox">

                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">افزودن دوره جدید</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <form action="#" class="form">
                                    <div class="row">
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row">
                                                <label>اسلاگ دوره</label>
                                                <input placeholder="اسلاگ دوره را وارد کنید " id="course_slug">
                                            </div>
                                        </div>
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row">
                                                <label>عنوان دوره</label>
                                                <input placeholder="عنوان دوره را وارد کنید " id="course_title">
                                            </div>
                                        </div>


                                        <div class="padding-item col-lg-12 col-md-12 col-sm-12 margin-top">
                                            <div class="border-item text-editor position-relative">
                                                <div id="meeting-editor" style="min-height: 600px; width: 100%">


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">اطلاعات کلی جلسه</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <form action="#" class="form">
                                    <div class="row">
                                        <div class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12">
                                            <div class="input-row">
                                                <label> قیمت دوره </label>
                                                <input
                                                    type="text"
                                                    name="currency-field"
                                                    data-type="currency"
                                                    placeholder="قیمت دوره را وارد کنید"
                                                    id="course_price"
                                                    style="direction: rtl!important;"
                                                />
                                                <div class="currency acsses size">
                                                    تومان
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12"
                                        >
                                            <div class="input-row">
                                                <label> قیمت با تخفیف </label>
                                                <input
                                                    type="text"
                                                    name="currency-field"
                                                    data-type="currency"
                                                    placeholder="قیمت با تخفیف دوره را وارد کنید"
                                                    id="course_discount_price"
                                                />
                                                <div class="currency acsses size">
                                                    تومان
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="margin-bottom padding-item col-lg-12 col-md-12 col-sm-12"
                                        >
                                            <div class="input-row time-row">
                                                <label> مدت زمان جلسه</label>
                                                <input
                                                    maxlength="8"
                                                    type="text"
                                                    name="duration-field"
                                                    id="duration-field"
                                                    data-type="duration"
                                                    placeholder="-:-:-"
                                                />

                                                <img
                                                    class="delet-value"
                                                    src="{{voyager_asset('icon/delet-value.svg')}}"
                                                />
                                            </div>
                                        </div>
                                        <div
                                            class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12"
                                        >
                                            <div class="input-row">
                                                <label> آپلود فایل تمرین </label>
                                                <label
                                                    id="filename"
                                                    for="upload-file"
                                                    class="upload-file menu-items flex-box"
                                                >
                                      <span class="flex-box">
                                        <img src="{{voyager_asset('icon/upload-file.svg')}}"/>
                                        فایل تمرین
                                      </span>
                                                    <p>انتخاب فایل</p>
                                                    <input
                                                        id="upload-file"
                                                        type="file"
                                                        class=""
                                                        multiple
                                                        hidden
                                                    />
                                                </label>
                                            </div>
                                            <div class="upload-file-row"></div>
                                        </div>
                                        <div
                                            class="padding-item margin-bottom col-lg-12 col-md-12 col-sm-12"
                                        >
                                            <div class="input-row">
                                                <label>آپلود تیزر </label>
                                                <label
                                                    id="fileimage"
                                                    for="upload-imge"
                                                    class="upload-file menu-items flex-box"
                                                >
                                      <span class="flex-box">
                                        <img
                                            src="{{voyager_asset('icon/upload-image.svg')}}"
                                        />
                                        تیزر
                                      </span>
                                                    <p>انتخاب ویدئو</p>
                                                    <input
                                                        id="upload-imge"
                                                        type="file"
                                                        class=""
                                                        multiple
                                                        hidden
                                                    />
                                                </label>
                                            </div>
                                            <div class="upload-image-row"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="row">
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">تصویر شاخص</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box">
                                <div class="edit-emage upload-image">
                                    <div class="imageWrapper">
                                        <img class="delete-image" src="{{voyager_asset('icon/delete-image.svg')}}"/>
                                        <img class="image" src="{{voyager_asset('photo/sample-image.png')}}"/>
                                        <div class="loading-box">
                                            <div class="circle">
                                                <div class="inner-circle"></div>
                                            </div>
                                            <div class="loading-container">
                                                <div class="loader">
                                                    <div class="loading-bubbles">
                                                        <div class="bubble-container">
                                                            <div class="bubble"></div>
                                                        </div>
                                                        <div class="bubble-container">
                                                            <div class="bubble"></div>
                                                        </div>
                                                        <div class="bubble-container">
                                                            <div class="bubble"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <p>در حال آپلود....</p>
                                        </div>
                                    </div>
                                    <button type="button" class="file-upload flex-box">
                                        <input type="file" class="file-input" id="add-image">
                                        <p>آپلود تصویر جدید</p>
                                        <div class="loading-container">
                                            <div class="loader">
                                                <div class="loading-bubbles">
                                                    <div class="bubble-container">
                                                        <div class="bubble"></div>
                                                    </div>
                                                    <div class="bubble-container">
                                                        <div class="bubble"></div>
                                                    </div>
                                                    <div class="bubble-container">
                                                        <div class="bubble"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">دسته بندی</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box has-scroll">
                                @foreach($categories as $category)
                                    <label class="form-control-squer has-border ">
                                        <div class="flex-box details-row ">
                                            <div class="flex-box image-box">
                                                @if($category->image)
                                                    <img src="{{ Voyager::image($category->image) }}"/>
                                                @else
                                                    <img src="{{voyager_asset('photo/sample-image.png')}}">

                                                @endif
                                            </div>
                                            <div>
                                                <div class="title-text">{{ $category->name }}</div>
                                                {{--                                            <div class="date">Motion-Design</div>--}}
                                            </div>
                                        </div>

                                        <input type="checkbox" class="categoryCheckbox" value="{{ $category->id }}"
                                               name="category">
                                    </label>
                                @endforeach
                            </div>
                        </div>


                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">مدرسین</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box has-scroll">
                                @foreach($teachers as $teacher)
                                    <label class="form-control-squer has-border ">
                                        <div class="flex-box details-row ">
                                            <div class="flex-box image-box">
                                                <img src="{{ Voyager::image($teacher->image) }}"/>
                                            </div>
                                            <div>
                                                <div class="title-text">{{ $teacher->name }}</div>
                                                {{--                                            <div class="date">Udemy</div>--}}
                                            </div>
                                        </div>
                                        <input type="checkbox" class="teacherCheckbox" value="{{ $teacher->id }}"
                                               name="teacher">
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">ابزار ها</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box has-scroll">
                                @foreach($tools as $tool)
                                    <label class="form-control-squer has-border ">
                                        <div class="flex-box details-row ">
                                            <div class="flex-box image-box">
                                                <img src="{{ Voyager::image($tool->image) }}"/>
                                            </div>
                                            <div>
                                                <div class="title-text">{{ $tool->name }}</div>
                                                {{--                                            <div class="date">Figma</div>--}}
                                            </div>
                                        </div>
                                        <input type="checkbox" class="toolCheckbox" value="{{ $tool->id }}"
                                               name="tool">
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">پیش نیاز ها</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box has-scroll">
                                @foreach($courses as $course)
                                    <label class="form-control-squer has-border ">
                                        <div class="flex-box details-row ">
                                            <div class="flex-box image-box">
                                                <img src="{{ Voyager::image($course->image) }}"/>
                                            </div>
                                            <div>
                                                <div class="title-text">{{ $course->title }}</div>
                                                @if(count($course->category) > 0)
                                                    <div class="date">{{ $course->category->first()->name }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <input type="checkbox" class="courseCheckbox" value="{{ $course->id }}"
                                               name="course">
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="title">توضیحات سایدبار</div>
                        </div>
                        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                            <div class="box padding">
                                <form>
                                    <div class="input-row">
                                        <label>
                                            توضیحات سایدبار
                                        </label>
                                        <textarea placeholder="اینپوت متن توضیح" id="sidebar_text"></textarea>


                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')
    <script>
        var editor1 = new RichTextEditor("#meeting-editor");
        editor1.execCommand("justifyright");
    </script>

    <script>
        var categoryArray = [];
        var teacherArray = [];
        var toolArray = [];
        var courseArray = [];
        var editor1Code = '';

        function submit_button() {
            const title = $('#course_title').val();
            const slug = $('#course_slug').val();
            const price = $('#course_price').val();
            const discount_price = $('#course_discount_price').val();
            const course_time = $('#duration-field').val();
            const sidebar = $('#sidebar_text').val();
            var featured_image = $("#add-image")[0].files[0];
            var video_file = $("#upload-file")[0].files[0];
            var image_file = $("#upload-imge")[0].files[0];

            $(".categoryCheckbox:checked").each(function () {
                categoryArray.push($(this).val());
            });

            $(".teacherCheckbox:checked").each(function () {
                teacherArray.push($(this).val());
            });

            $(".toolCheckbox:checked").each(function () {
                toolArray.push($(this).val());
            });

            $(".courseCheckbox:checked").each(function () {
                courseArray.push($(this).val());
            });

            editor1Code = editor1.getHTMLCode();

            let form = new FormData();
            form.append('title', title);
            form.append('slug', slug);
            form.append('price', price);
            form.append('discount_price', discount_price);
            form.append('course_time', course_time);
            form.append('sidebar', sidebar);
            form.append('description', editor1Code);
            form.append('featured_image', featured_image);
            form.append('video_file', video_file);
            form.append('image_file', image_file);
            form.append('categories', categoryArray);
            form.append('teachers', teacherArray);
            form.append('tools', toolArray);
            form.append('precourses', courseArray);


            $.ajax({
                url: "{{ route('create_course.store') }}",
                method: "post",
                cache: false,
                contentType: false,
                processData: false,
                data: form,

                success: function (data) {
                    console.log(data);
                    window.location.replace("/kcp/courses");

                },
                error: function (error) {
                    var pageError = error.responseJSON.errors;


                    var errorBox = $(".errorBox");
                    errorBox.empty();
                    $.each(pageError, function (brand) {
                        errorBox.append(`
                            <div class="errorTextShow">${pageError[brand]}</div>
                        `);
                    });

                    $(errorBox).slideDown(400);


                }
            });
            categoryArray = [];
        }
    </script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/themes/nano.min.css"/>
    <link href="{{ voyager_asset('css/jalalidatepicker.min.css') }}" rel="stylesheet">


    {{--    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.min.js"></script>--}}
    {{--    <script src="https://cdn.jsdelivr.net/npm/@simonwep/pickr/dist/pickr.es5.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>--}}
    {{--    <script src="{{ voyager_asset('js/jalalidatepicker.min.js') }}"></script>--}}


    <script>
        $(".file-input").change(function () {
            var curElement = $(".image");
            var reader = new FileReader();
            reader.onload = function (e) {
                // get loaded data and render thumbnail.
                curElement.attr("src", e.target.result);
                $(".loading-box").addClass("active");
                $(".file-upload").addClass("active");
                setTimeout(function () {
                    $(".loading-box").removeClass("active");
                    $(".file-upload").removeClass("active");
                }, 3000);
            };
            // read the image file as a data URL.

            reader.readAsDataURL(this.files[0]);
        });
        $(".delete-image").click(function () {
            var curElement = $(".image");
            curElement.attr("src", "assets/photo/sample-image.png");
        });
    </script>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var $googleURL = e.target.result;
                    $(".upload-image-row")
                        .append(
                            '<div class="acsses image-file files">' +
                            '<div class="flex-box">' +
                            "<img class='video-tag' src=\"assets/icon/video.svg\">" +
                            "<img class='close-tag' src=\"assets/icon/clos.svg\">" +
                            "<div class='image-box'>" +
                            "<img id='upload-image-item' src='" +
                            $googleURL +
                            "'>" +
                            "</div>" +
                            "</div>" +
                            '<div class="flex-box upload-prosses">' +
                            "<p>در حال آپلود..</p>" +
                            '<p class="blue-text">۱۰ ٪</p>' +
                            "</div>" +
                            "</div>"
                        )
                        .addClass("margin-bottom");
                    $(".close-tag").click(function () {
                        $(this).parent().parent().fadeOut();
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#upload-imge").change(function () {
            readURL(this);
        });
    </script>


    <script>
        $(".delet-value").on("click", function () {
            $(this).parent("div").find("input").val("");
        });
    </script>


    <script>
        $(".input-row").each(function () {
            var spinner = $(this),
                input = spinner.find('input[type="number"]'),
                btnUp = spinner.find(".quantity__btn--up"),
                btnDown = spinner.find(".quantity__btn--down"),
                min = input.attr("min"),
                max = input.attr("max");

            btnUp.click(function () {
                var oldValue = parseFloat(input.val());
                if (oldValue >= max) {
                    var newVal = oldValue;
                } else {
                    var newVal = oldValue + 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });

            btnDown.click(function () {
                ConvertNumberToPersion();
                var oldValue = parseFloat(input.val());
                if (oldValue <= min) {
                    ConvertNumberToPersion();

                    var newVal = oldValue;
                } else {
                    ConvertNumberToPersion();

                    var newVal = oldValue - 1;
                }
                spinner.find("input").val(newVal);
                spinner.find("input").trigger("change");
            });
        });
    </script>
    <script>
        // Jquery Dependency

        $("input[data-type='duration']").on({
            keyup: function () {
                formatDuration($(this));
            },
        });

        function formatTime(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{2})+(?!\d))/g, ":");
        }

        function formatDuration(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.

            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") {
                return;
            }

            // original length
            var original_len = input_val.length;

            // initial caret position
            var caret_pos = input.prop("selectionStart");

            // check for decimal
            if (input_val.indexOf(".") >= 0) {
                // get position of first decimal
                // this prevents multiple decimals from
                // being entered
                var decimal_pos = input_val.indexOf(".");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatTime(left_side);

                // validate right side
                right_side = formatTime(right_side);

                // On blur make sure 2 numbers after decimal
                if (blur === "blur") {
                    right_side += "00";
                }

                // Limit decimal to only 2 digits
                right_side = right_side.substring(0, 2);

                // join number by .
                input_val = "$" + left_side + "." + right_side;
            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatTime(input_val);

                // final formatting
                if (blur === "blur") {
                }
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }
    </script>
    <script>
        // Jquery Dependency

        $("input[data-type='currency']").on({
            keyup: function () {
                formatCurrency($(this));
            },
            blur: function () {
                formatCurrency($(this), "blur");
            },
        });

        function formatNumber(n) {
            // format number 1000000 to 1,234,567
            return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function formatCurrency(input, blur) {
            // appends $ to value, validates decimal side
            // and puts cursor back in right position.

            // get input value
            var input_val = input.val();

            // don't validate empty input
            if (input_val === "") {
                return;
            }

            // original length
            var original_len = input_val.length;

            // initial caret position
            var caret_pos = input.prop("selectionStart");

            // check for decimal
            if (input_val.indexOf(".") >= 0) {
                // get position of first decimal
                // this prevents multiple decimals from
                // being entered
                var decimal_pos = input_val.indexOf(".");

                // split number by decimal point
                var left_side = input_val.substring(0, decimal_pos);
                var right_side = input_val.substring(decimal_pos);

                // add commas to left side of number
                left_side = formatNumber(left_side);

                // validate right side
                right_side = formatNumber(right_side);

                // On blur make sure 2 numbers after decimal
                if (blur === "blur") {
                    right_side += "00";
                }

                // Limit decimal to only 2 digits
                right_side = right_side.substring(0, 2);

                // join number by .
                input_val = "$" + left_side + "." + right_side;
            } else {
                // no decimal entered
                // add commas to number
                // remove all non-digits
                input_val = formatNumber(input_val);

                // final formatting
                if (blur === "blur") {
                }
            }

            // send updated string to input
            input.val(input_val);

            // put caret back in the right position
            var updated_len = input_val.length;
            caret_pos = updated_len - original_len + caret_pos;
            input[0].setSelectionRange(caret_pos, caret_pos);
        }
    </script>
    <script>
        $("#upload-file").on("change", function () {
            var filename = $(this)
                .val()
                .replace(/.*(\/|\\)/, "");
            $(".upload-file-row").append(
                '<div class="acsses  files">' +
                '<div class="flex-box">' +
                "<img class='close-tag' src=\"assets/icon/clos.svg\">" +
                filename +
                "</div>" +
                '<div class="flex-box upload-prosses">' +
                "<p>در حال آپلود..</p>" +
                '<p class="blue-text">۱۰ ٪</p>' +
                "</div>" +
                "</div>"
            );
            $(".close-tag").click(function () {
                $(this).parent().parent().fadeOut();
            });
        });
    </script>
@endsection
