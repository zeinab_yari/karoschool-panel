@php
    $notifications = \TCG\Voyager\Facades\Notification::notification()->all();
@endphp
@if($notifications->count() > 0)
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row">
            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <h5 class="title">
                    {{  __('voyager::hotdesk.notifications') }}
                </h5>
            </div>
            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <div class="box">
                    <div class="row">
                        @foreach($notifications as $notif)
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <a class="system-details flex-box">
                                    @if($notif->type == 'product')
                                        <div class="red flex-box image-box">
                                            <img src="{{ voyager_asset('icon/basket.svg') }}">
                                        </div>
                                    @elseif($notif->type == 'client')
                                        <div class="blue flex-box image-box">
                                            <img src="{{ voyager_asset('icon/security.svg') }}">
                                        </div>
                                    @elseif($notif->type == 'comment')
                                        <div class="green-2  flex-box image-box">
                                            <img src="{{ voyager_asset('icon/massages.svg') }}">
                                        </div>
                                    @elseif($notif->type == 'support')
                                        <div class="yellow flex-box image-box">
                                            <img src="{{ voyager_asset('icon/conversation.svg') }}">
                                        </div>
                                    @endif


                                    <div>
                                        <p>
                                            {{ $notif->value }}
                                        </p>
                                        <p class="date">
                                            @if($notif->updated_at)
                                                {{ $notif->updated_at }}
                                            @else
                                                {{ $notif->created_at }}
                                            @endif

                                        </p>
                                    </div>

                                </a>
                            </div>
                        @endforeach

                    </div>

                </div>
            </div>
        </div>
    </div>
@endif

