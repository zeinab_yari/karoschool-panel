<div class="col-lg-4 col-md-12 col-sm-12">
    <div class="row">
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <h5 class="title">
                {{__('voyager::hotdesk.notes')}}
            </h5>
        </div>
        <div class="padding-item col-lg-12 col-md-12 col-sm-12">
            <div class="box">
                <form id="note_form" class="padding-item" action="/kcp/notes/set" method="POST">
                    @csrf
                    <textarea name="content" class="send-massage"
                              placeholder="{{__('voyager::hotdesk.notes_text')}} "></textarea>
                    <div class="flex-box massage-details">
                        <div class="flex-box massage-situation">
                            <p>
                                {{__('voyager::hotdesk.notes_status')}}
                            </p>
                            <span class="yellow outer flex-box yellow-border active"
                                  onclick="$('#status_of_note').val('yellow')"><span class="yellow"></span></span>
                            <span class="green outer flex-box green-border" onclick="$('#status_of_note').val('green')"><span
                                    class="green-2"></span></span>
                            <span class="red outer flex-box red-border" onclick="$('#status_of_note').val('red')"><span
                                    class="red"></span></span>
                            <input type="hidden" name="status" value="yellow" id="status_of_note"/>
                        </div>
                        <img class="" onclick="document.getElementById('note_form').submit()"
                             src="{{ voyager_asset('icon/send-massage.svg') }}">
                    </div>
                </form>
            </div>
        </div>
        @if(count(Auth::user()->notes()->get()) > 0)
            <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                <div class="flex-box massage-details">
                    <div class="flex-box">
                        <p class="active">
                            {{__('voyager::hotdesk.notes_list')}}
                        </p>

                    </div>
                    <div class="massage-filter flex-box massage-situation">
                    <span class="yellow outer flex-box yellow-border" onclick="filterNotes('yellow')">
                        <span class="yellow"></span>
                    </span>
                        <span class="green outer flex-box green-border" onclick="filterNotes('green')">
                        <span class="green-2"></span>
                    </span>
                        <span class="red outer flex-box red-border" onclick="filterNotes('red')">
                        <span class="red"></span>
                    </span>
                    </div>

                </div>
            </div>
        @endif
        <div class=" col-lg-12 col-md-12 col-sm-12">
            <div class="row massage-row">
                @foreach(Auth::user()->notes()->get() as $note)
                    @php
                        $user = $note->user()->first();
                        if($note->user()->exists()){
                          if (\Illuminate\Support\Str::startsWith($user->avatar, 'http://') || \Illuminate\Support\Str::startsWith($user->avatar, 'https://')) {
                              $user_avatar = $user->avatar;
                          } else {
                              $user_avatar = Voyager::image($user->avatar);
                          }
                        } else {
                          $user_avatar =  voyager_asset('icon/karo.svg');
                        }
                    @endphp
                    <div class="padding-item col-lg-12 col-md-12 col-sm-12 showMessageBox">
                        <div class="box massage-box">
                            <p>{{$note->content}}</p>
                            <input class="remove_note_input" onclick="delete_note({{ $note->id }})"
                                   type="checkbox" id="{{ $note->status }}-massage-{{ $loop->index }}"><label
                                for="{{ $note->status }}-massage-{{ $loop->index }}" data-id="{{ $note->id }}"><img
                                    src="{{ voyager_asset('icon/check.svg') }}"></label>
                            <div class="extra {{ $note->status }}"></div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>

        <script>

            function delete_note(id) {

                $.post("/kcp/notes/delete", {
                    id: id,
                    _token: "{{ csrf_token() }}"
                });
                $(this).parents('.item').fadeOut(500, function () {
                    $(this).remove();
                    $(".layout .main .notes .notelist .list").mCustomScrollbar('update');
                });
            }

            let massageBoxArray = [];

            $(document).ready(function () {
                $(".massage-box").each(function () {
                    let messageDetail = {text: "", delId: "", delFuc: "", id: "", color: ""}
                    messageDetail.text = $(this).children("p").text();
                    messageDetail.delId = $(this).children("input").attr("id");
                    messageDetail.delFuc = $(this).children("input").attr("onclick");
                    messageDetail.id = $(this).children("label").data("id");
                    messageDetail.color = $(this).children("div").attr("class").split(" ")[1];
                    massageBoxArray.push(messageDetail)
                })


            })


            function filterNotes(color) {
                let baseEle = $(".massage-row");
                $(baseEle).empty();
                for (let i = 0; massageBoxArray.length > i; i++) {
                    const message = massageBoxArray[i];
                    if (message.color == color) {

                        $(baseEle).append(`
                            <div class="padding-item col-lg-12 col-md-12 col-sm-12 showMessageBox">
                                    <div class="box massage-box">
                                        <p>${message.text}</p>
                                        <input class="remove_note_input" onclick="${message.delFuc}" type="checkbox" id="${message.id}">
                                        <label for="green-massage-1" data-id="4">
                                            <img src="/kcp/voyager-assets?path=icon%2Fcheck.svg">
                                        </label>
                                        <div class="extra ${message.color}"></div>
                                    </div>
                            </div>
                        `)
                    }
                }
                $(".layout .main .notes .notelist .list").mCustomScrollbar('update');

            }

        </script>

    </div>
</div>
