@php
    $relationshipDetails = $relationship['details'];
    $relationshipKeyArray = array_fill_keys(["model", "table", "type", "column", "key", "label", "pivot_table", "pivot", "taggable"], '');

    $adv_details = array_diff_key(json_decode(json_encode($relationshipDetails), true), $relationshipKeyArray);

@endphp

<div class="tr row row-dd" style="border: solid 1px red;">
    <div class="padding-item col-md-3 col-xs-3" data-column="جزئیات غیراجباری">

        {{--<div class=" input-row-value">
            <textarea
                id="json-input-{{ ($relationship['field']) }}"
                class="resizable-editor" data-editor="json"
                name="field_details_{{ $relationship['field'] }}">
                         @if(!empty($adv_details))
                    {{ json_encode($adv_details) }}
                @else
                    {}
                @endif
                </textarea>
        </div>--}}
    </div>
    <div class="padding-item col-md-2 col-xs-2" data-column="نام نمایشی">
        <div class=" input-row">
            @if($isModelTranslatable)
                @include('voyager::multilingual.input-hidden', [
                    'isModelTranslatable' => true,
                    '_field_name'         => 'field_display_name_' . $relationship['field'],
                    '_field_trans' => get_field_translations($relationship, 'display_name')
                ])
            @endif
            <input type="text" name="field_display_name_{{ $relationship['field'] }}"
                   value="{{ $relationship['display_name'] }}">
        </div>
    </div>
    <div class="padding-item col-md-2 col-xs-2" data-column=" نوع ورودی">
        <div class=" input-row">
            <p>{{ __('voyager::database.relationship.relationship') }}</p>
        </div>
    </div>
    <div class="padding-item col-md-2 col-xs-2" data-column="وضعیت نمایش">

        <div class="flex-box">
            <div class="check-box edit-checkbox">
                <input class="my-checkbox" type="checkbox" id="field_browse_{{ $relationship['field'] }}" name="field_browse_{{ $relationship['field'] }}"
                       @if(isset($relationship->browse) && $relationship->browse) checked="checked"
                       @elseif(!isset($relationship->browse)) checked="checked" @endif>
                <label for="field_browse_{{ $relationship['field'] }}">
                    <img src="{{voyager_asset('icon/white-checkbox.svg')}}">
                </label>
            </div>
            <label for="field_browse_{{ $relationship['field'] }}">
                {{ __('voyager::database.relationship.browse') }}
            </label>
        </div>

        <div class="flex-box">
            <div class="check-box edit-checkbox">
                <input class="my-checkbox" type="checkbox" id="field_read_{{ $relationship['field'] }}" name="field_read_{{ $relationship['field'] }}"
                       @if(isset($relationship->read) && $relationship->read) checked="checked"
                       @elseif(!isset($relationship->browse)) checked="checked" @endif>
                <label for="field_read_{{ $relationship['field'] }}">
                    <img src="{{voyager_asset('icon/white-checkbox.svg')}}">
                </label>
            </div>
            <label for="field_read_{{ $relationship['field'] }}">
                {{ __('voyager::database.relationship.read') }}
            </label>
        </div>


        <div class="flex-box">
            <div class="check-box edit-checkbox">
                <input class="my-checkbox" type="checkbox" id="field_edit_{{ $relationship['field'] }}" name="field_edit_{{ $relationship['field'] }}"
                       @if(isset($relationship->edit) && $relationship->edit) checked="checked"
                       @elseif(!isset($relationship->edit)) checked="checked" @endif>
                <label for="field_edit_{{ $relationship['field'] }}">
                    <img src="{{voyager_asset('icon/white-checkbox.svg')}}">
                </label>
            </div>
            <label for="field_edit_{{ $relationship['field'] }}">
                {{ __('voyager::database.relationship.edit') }}
            </label>
        </div>

        <div class="flex-box">
            <div class="check-box edit-checkbox">
                <input class="my-checkbox" type="checkbox" id="field_add_{{ $relationship['field'] }}" name="field_add_{{ $relationship['field'] }}"
                       @if(isset($relationship->add) && $relationship->add) checked="checked"
                       @elseif(!isset($relationship->add)) checked="checked" @endif>
                <label for="field_add_{{ $relationship['field'] }}">
                    <img src="{{voyager_asset('icon/white-checkbox.svg')}}">
                </label>
            </div>
            <label for="field_add_{{ $relationship['field'] }}">
                {{ __('voyager::database.relationship.add') }}
            </label>
        </div>


        <div class="flex-box">
            <div class="check-box edit-checkbox">
                <input class="my-checkbox" type="checkbox" id="field_delete_{{ $relationship['field'] }}" name="field_delete_{{ $relationship['field'] }}"
                       @if(isset($relationship->delete) && $relationship->delete) checked="checked"
                       @elseif(!isset($relationship->delete)) checked="checked" @endif>
                <label for="field_delete_{{ $relationship['field'] }}">
                    <img src="{{voyager_asset('icon/white-checkbox.svg')}}">
                </label>
            </div>
            <label for="field_delete_{{ $relationship['field'] }}">
                {{ __('voyager::database.relationship.delete') }}
            </label>
        </div>

    </div>
    <div class="padding-item col-md-2 col-xs-2 text-center" data-column="فیلد">
        <p>
            {{ $relationship->getTranslatedAttribute('display_name') }}
        </p>
        <p>
            {{ __('voyager::database.type') }}:
            {{ __('voyager::database.relationship.relationship') }}
        </p>
        <input class="row_order" type="hidden" value="{{ $relationship['order'] }}"
               name="field_order_{{ $relationship['field'] }}">

    </div>
    <div class="padding-item col-md-1 col-xs-1 text-center dragBreadMain" >
       {{-- <a class="flex-box drag-item">
            <img src="{{voyager_asset('icon/Hand,.svg')}}">

        </a>--}}
        <div class="handler voyager-handle drag-item">
            <img src="{{voyager_asset('icon/Hand,.svg')}}"/>
        </div>

        <input class="row_order" type="hidden"
               value="{{ $dataRow->order ?? $r_order }}"
               name="field_order_{{ $data['field'] }}">

    </div>

    <div class="tr" style="">
        <div style="width: 100%">

            <div class="col-xs-4 mt-2">
                <div class="voyager-relationship-details-btn open">
                    <i class="voyager-angle-down"></i><i class="voyager-angle-up"></i>
                    <span class="open_text">{{ __('voyager::database.relationship.open') }}</span>
                    <span class="close_text">{{ __('voyager::database.relationship.close') }}</span>
                    {{ __('voyager::database.relationship.relationship_details') }}
                </div>
            </div>
            <div class="col-md-12 mt-3 voyager-relationship-details ">

                <a href="{{ route('voyager.bread.delete_relationship', $relationship['id']) }}" class="delete_relationship">
                    <i class="voyager-trash"></i> {{ __('voyager::database.relationship.delete') }}</a>

                <div class="input-row ">
                    <p class="relationship_table_select">{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</p>
                    <div class="newRelationshow ">
                        <select class="relationship_type select2 newRelationshowCard" name="relationship_type_{{ $relationship['field'] }}">
                            <option value="hasOne" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasOne') selected="selected" @endif>{{ __('voyager::database.relationship.has_one') }}</option>
                            <option value="hasMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'hasMany') selected="selected" @endif>{{ __('voyager::database.relationship.has_many') }}</option>
                            <option value="belongsTo" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsTo') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to') }}</option>
                            <option value="belongsToMany" @if(isset($relationshipDetails->type) && $relationshipDetails->type == 'belongsToMany') selected="selected" @endif>{{ __('voyager::database.relationship.belongs_to_many') }}</option>
                        </select>
                        <select class="relationship_table select2 newRelationshowCard" name="relationship_table_{{ $relationship['field'] }}">
                            @foreach($tables as $tablename)
                                <option value="{{ $tablename }}" @if(isset($relationshipDetails->table) && $relationshipDetails->table == $tablename) selected="selected" @endif>{{ ucfirst($tablename) }}</option>
                            @endforeach
                        </select>
                        <span class="newRelationshowCard">
                        <input type="text" class="form-control" name="relationship_model_{{ $relationship['field'] }}"
                               placeholder="{{ __('voyager::database.relationship.namespace') }}" value="{{ $relationshipDetails->model ?? '' }}">
                    </span>
                    </div>
                </div>

                <div class="relationshipField">
                    <div class="relationship_details_content margin_top belongsTo customRelationshipBox @if($relationshipDetails->type == 'belongsTo') flexed @endif">
                        <label>{{ __('voyager::database.relationship.which_column_from') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span class="label_table_name"></span>?</label>
                        <select name="relationship_column_belongs_to_{{ $relationship['field'] }}" class="new_relationship_field select2 selectRelationTemp">
                            @foreach($fieldOptions as $data)
                                <option value="{{ $data['field'] }}" @if($relationshipDetails->column == $data['field']) selected="selected" @endif>{{ $data['field'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="relationship_details_content margin_top hasOneMany customRelationshipBox @if($relationshipDetails->type == 'hasOne' || $relationshipDetails->type == 'hasMany') flexed @endif">
                        <label>{{ __('voyager::database.relationship.which_column_from') }} <span class="label_table_name"></span> {{ __('voyager::database.relationship.is_used_to_reference') }} <span>{{ \Illuminate\Support\Str::singular(ucfirst($table)) }}</span>?</label>
                        <select name="relationship_column_{{ $relationship['field'] }}" class="new_relationship_field select2 selectRelationTemp rowDrop" data-table="{{ $relationshipDetails->table ?? '' }}" data-selected="{{ $relationshipDetails->column }}">
                        </select>
                    </div>
                </div>
                <div class="relationship_details_content margin_top relationshipPivot customRelationshipBox @if($relationshipDetails->type == 'belongsToMany') visible @endif">
                    <label>{{ __('voyager::database.relationship.pivot_table') }}:</label>
                    <select name="relationship_pivot_table_{{ $relationship['field'] }}" class="select2 selectRelationTemp">
                        @foreach($tables as $tbl)
                            <option value="{{ $tbl }}" @if(isset($relationshipDetails->pivot_table) && $relationshipDetails->pivot_table == $tbl) selected="selected" @endif>{{ \Illuminate\Support\Str::singular(ucfirst($tbl)) }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="relationship_details_content margin_top customRelationshipBox">
                    <label>{{ __('voyager::database.relationship.display_the') }} <span class="label_table_name"></span></label>
                    <select name="relationship_label_{{ $relationship['field'] }}" class="rowDrop select2 selectRelationTemp" data-table="{{ $relationshipDetails->table ?? '' }}" data-selected="{{ $relationshipDetails->label ?? ''}}">
                    </select>
                    <div class="px-1"></div>
                    <label class="relationship_key" style="@if($relationshipDetails->type == 'belongsTo' || $relationshipDetails->type == 'belongsToMany') display:block @endif">{{ __('voyager::database.relationship.store_the') }} <span class="label_table_name"></span></label>
                    <select name="relationship_key_{{ $relationship['field'] }}" class="rowDrop select2 relationship_key selectRelationTemp" style="@if($relationshipDetails->type == 'belongsTo' || $relationshipDetails->type == 'belongsToMany') display:block @endif" data-table="@if(isset($relationshipDetails->table)){{ $relationshipDetails->table }}@endif" data-selected="@if(isset($relationshipDetails->key)){{ $relationshipDetails->key }}@endif">
                    </select>
                    <br>

                </div>
                @isset($relationshipDetails->taggable)
                    <div id="check-box-row" class="flex-box check-box-row">

                        <label class="switch">
                            <h6>
                                {{__('voyager::database.relationship.allow_tagging')}}
                            </h6>
                            <input type="checkbox" name="relationship_taggable_{{ $relationship['field'] }}" class=""
                                   data-on="{{ __('voyager::generic.yes') }}" data-off="{{ __('voyager::generic.no') }}"
                                {{$relationshipDetails->taggable == 'on' ? 'checked' : ''}} hidden />
                            <span class="slider round"></span>
                        </label>
                    </div>

                    {{--<label class="relationship_taggable" style="@if($relationshipDetails->type == 'belongsToMany') display:block @endif">
                        {{__('voyager::database.relationship.allow_tagging')}}
                    </label>
                    <span class="relationship_taggable" style="@if($relationshipDetails->type == 'belongsToMany') display:block @endif">
                        <input type="checkbox" name="relationship_taggable_{{ $relationship['field'] }}" class="toggleswitch"
                               data-on="{{ __('voyager::generic.yes') }}" data-off="{{ __('voyager::generic.no') }}"
                            {{$relationshipDetails->taggable == 'on' ? 'checked' : ''}}>
                    </span>--}}
                @endisset
            </div>

            <div class="relationship_details_content margin_top">
                <div class="col-xs-12" style="margin: 0px !important; padding: 0px !important;">
                    <div class="alert alert-danger validation-error">
                        {{ __('voyager::json.invalid') }}
                    </div>
                    <label>{{ __('voyager::database.relationship.relationship_details') }}</label>
                    <textarea id="json-input-{{ ($relationship['field']) }}" class="resizable-editor" data-editor="json"
                              name="field_details_{{ $relationship['field'] }}">
                    @if(!empty($adv_details))
                            {{ json_encode($adv_details) }}
                        @else
                            {}
                        @endif
                </textarea>
                </div>
            </div>


            <input type="hidden" value="0" name="field_required_{{ $relationship['field'] }}" checked="checked">
            <input type="hidden" name="field_input_type_{{ $relationship['field'] }}" value="relationship">
            <input type="hidden" name="field_{{ $relationship['field'] }}" value="{{ $relationship['field'] }}">
            <input type="hidden" name="relationships[]" value="{{ $relationship['field'] }}">

        </div>

    </div>

</div>

