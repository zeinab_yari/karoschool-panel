@if(isset($options->model) && isset($options->type))

    @if(class_exists($options->model))

        @php $relationshipField = $row->field; @endphp

        @if($options->type == 'belongsToMany')

            @if(isset($view) && ($view == 'browse' || $view == 'read'))

                @php
                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;

                    $selected_values = isset($relationshipData) ? $relationshipData->belongsToMany($options->model, $options->pivot_table, $options->foreign_pivot_key ?? null, $options->related_pivot_key ?? null, $options->parent_key ?? null, $options->key)->get()->map(function ($item, $key) use ($options) {
                    return $item->{$options->label};
                    })->all() : array();
                @endphp

                @if($view == 'browse')
                    @php
                        $string_values =  $selected_values;

                             if ($options->label !='image'){
                                $string_values = implode(", ", $selected_values);

                                    if(mb_strlen($string_values) > 25){ $string_values = mb_substr($string_values, 0, 25) . '...'; }
                                }

                    @endphp
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        @if($options->label == 'image')
                            @foreach($selected_values as $selected_value)

                                <img src="{{ Voyager::image( $selected_value) }}" style="width: 30px">
                            @endforeach
                        @else
                            <p>{{ $string_values }}</p>
                        @endif
                    @endif
                @else
                    @if(empty($selected_values))
                        <p>{{ __('voyager::generic.no_results') }}</p>
                    @else
                        <ul>
                            @foreach($selected_values as $selected_value)
                                @if($options->label == 'image')
                                    <img src="{{ Voyager::image( $selected_value) }}">
                                @else
                                    <li>{{ $selected_value }}</li>
                                @endif
                            @endforeach
                        </ul>
                    @endif
                @endif

            @else

                @php
                    $selected_values = isset($dataTypeContent) ? $dataTypeContent->belongsToMany($options->model, $options->pivot_table, $options->foreign_pivot_key ?? null, $options->related_pivot_key ?? null, $options->parent_key ?? null, $options->key)->get()->map(function ($item, $key) use ($options) {
                        return $item->{$options->key};
                    })->all() : array();
                    $relationshipOptions = app($options->model)->all();
                    $selected_values = old($relationshipField, $selected_values);
                @endphp


                <div class="padding-item col-lg-12 col-md-12 col-sm-12">
                    <div class="box has-scroll">
                        @foreach($relationshipOptions as $relationshipOption)

                            <label class="form-control-squer has-border ">
                                <div class="flex-box details-row ">
                                    <div class="flex-box image-box">
                                        @if($relationshipOption->image)
                                            <img src="{{ Voyager::image($relationshipOption->image) }}"/>
                                        @else
                                            <img src="{{voyager_asset('photo/sample-image.png')}}">

                                        @endif
                                    </div>
                                    <div>
                                        <div class="title-text">{{ $relationshipOption->name }}</div>
                                        {{--                                            <div class="date">Motion-Design</div>--}}
                                    </div>
                                </div>

                                <input type="checkbox" class="categoryCheckbox" value="{{ $relationshipOption->id }}"
                                       name="{{ $relationshipField }}[]"
                                       data-get-items-route="{{route('voyager.' . $dataType->slug.'.relation')}}"
                                       data-get-items-field="{{$row->field}}"
                                       data-method="{{ !is_null($dataTypeContent->getKey()) ? 'edit' : 'add' }}"

                                       @if(in_array($relationshipOption->{$options->key}, $selected_values)) checked @endif />
                            </label>

                        @endforeach
                    </div>
                </div>
            @endif
        @endif
    @endif

@endif
